class Auto {
    constructor() {
      this.Raeder;
      this.Sitze;
    }
  }

class Schüler {
    constructor(){
    this.vorname
    this.nachname
    this.schuelerId
    }
}

class konto{
    constructor(){
        this.kontonummer
        this.kontoart
        this.kontoname
        this.username
        this.passwort
        this.IdKunde
        this.Geburtsdatum
        this.Adresse
        this.Telefon
    }
}

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.set('views', 'Training')

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

// Eine Klasse ist ein Bauplan. 
// Ein objekt ist die konkrete Umsetzung auf der Grundlage des Bauplans.
// Alle Objekte eines Buplans haben dieselben Eigenschaften, aber
// möglicherweise unterschiedleiche Eigenschaftswerte.

//KLassendefinition
//*********************

class Rechteck{
    constructor(){
        this.laenge
        this.breite
    }
}

class Schueler{
    constructor(){
        this.geschlecht
        this.vorname
        this.alter
    }
}

class Spieler{
    constructor(){
        this.name
        this.alter
        this.nationalitaet
        this.verein
    }
}

class Stift{
    constructor(){
        this.marke
        this.art
        this.farbe
    }
}

// Deklaration eines Rechteck-Objekts vom Typ Rechteck
// Deklaration = Bekanntmachung, dass es ein Objekt vom Typ Rechteck geben soll

// let rechteck = ...

// Instanziierung erkennt man am reservierten Wort "new".
// Instanziierung reserviert Speicherzellen für das soeben deklarierte Objekt.

// ... = new Rechteck()

// Initialisierung belegt die reservierten Speicherzellen mit konkreten
// Eigenschaftswerten.

// rechteck.breite = 3

let rechteck = new Rechteck()
rechteck.breite = 3
rechteck.laenge = 

console.log("Breite :" + rechteck.breite)
console.log("Länge :" + rechteck.breite)
console.log(rechteck)

let schueler = new Schueler()
schueler.geschlecht = "w"
schueler.vorname = "Leonie"
schueler.alter = 16

let spieler = new Spieler()
spieler.name = "Kylian Mbappé"
spieler.alter = "20"
spieler.nationalität = "Fr"
spieler.verein = "Paris Saint-Germain"
spieler.nummer = 10

let stift = new Stift()
stift.marke = "Lamy"
stift.art = "Füller"
stift.farbe = "lila"




// Wenn localhost:3000/klasse-objekt-ejs-trainieren aufgerufen wird ...

app.get('/klasse-objekt-ejs-trainieren',(req, res, next) => {   

    // ... wird klasse-objekt-ejs-trainieren.ejs gerendert:

    res.render('klasse-objekt-ejs-trainieren', {    
        breite : rechteck.breite,
        laenge : rechteck.laenge,   
        geschlecht : schueler.geschlecht,
        vorname : schueler.vorname,
        alter : schueler.alter,
        name : spieler.name,
        alter : spieler.alter,
        nationalitaet : spieler.nationalitaet,
        verein : spieler.verein,
        nummer : spieler.nummer,
        marke : stift.marke,
        art : stift.art,
        farbe : stift.farbe


    })
})
